package concept;

class Superclass{
	Superclass(){
		System.out.println("superclass : constructor");//第5個跑的
	}
}

class Subclass extends Superclass{
	static final int STATIC_FINAL = 47;
	static final int STATIC_FINAL2 = (int)(Math.random()*5);
	static {
		System.out.println("Subclass : static initializer");//第3個跑的
	}
	Subclass(){
		System.out.println("Subclass : constructor");//第6個跑的
	}
}

public class ClassInitializationDemo {

	static {
		System.out.println("ClassInitializationDemo : static initializer");//第1個跑的
	}
	
	{System.out.println("ClassInitializationDemo : instance initializer");}
	public static void main(String[] args) {
		System.out.println("Subclass.STATIC_FINAL : "+ Subclass.STATIC_FINAL);//第2個跑的
		System.out.println("Subclass.STATIC_FINAL2 : "+ Subclass.STATIC_FINAL2);//第4個跑的
		new Subclass();
	}

}
